import React from "react";

import styles from "./index.scss";

export default class RenameSaveButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isPending: false};
    }

    action(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            isPending: true
        });
        setTimeout(() => {
            this.props.renameItem().then(() => {
                console.log("finish update");
            }).catch((e) => {
                this.setState({
                    isPending: false
                });
            });

        }, 1500)//pretend ajax request will take no less 1.5s

    }

    render() {
        let isPending = this.state.isPending;
        const el = isPending ? <span>Loading…</span> :
            <button onClick={(e) => this.action(e)}>Save</button>;

        return (
            <div className={styles['btn-container']}>
                {el}
            </div>
        )
    }

}
