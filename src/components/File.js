import React from "react";

import styles from "./File.scss";
import ListItem from "./list.item";

export default function File({name,removeItem,renameItem}) {
    return <div className={styles.file} >
       <ListItem name={name} removeItem={removeItem}renameItem={renameItem}/>
    </div>;
}
