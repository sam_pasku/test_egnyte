import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import cls from "classnames";

import BackButton from "../components/BackButton";
import File from "../components/File";
import Folder from "../components/Folder";
import Title from "../components/Title";

import grid from "getbase/src/scss/styles.scss";
import listStyles from "./List.scss";
import {ErrorMessage} from "../components/error";

export class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isPending: new Date(), error: null};
    }

    componentWillMount() {
        this.props.fetchRootData();
    }

    async removeFolderItem(item) {
        await this.props.removeFolder(item.id);
    }

    async removeFileItem(id) {
        await this.props.removeFile(id);
    }

    async renameFolderItem(id, name,oldName) {
        try {
            return await this.props.renameFolder({id, name});
        } catch (e) {
            this.catchError(e, 'Folder', oldName);
        }

    }

    catchError(e, type, val) {
        this.setState({error: `${type} ${val} rename failed.`});
        setTimeout(() => {
            this.setState({error: null});
        }, 2500);
        throw new Error(e);
    }

    async renameFileItem(id, name,oldName) {
        try {
            return await this.props.renameFile({id, name});
        } catch (e) {
            this.catchError(e, 'Folder', oldName);
        }

    }

    render() {
        const {loadFolderDetails, goBack, info, folders, files} = this.props;
        const {error} = this.state;

        return (
            <div className={cls(grid["container-m"], listStyles.folderDetails)}>
                <ErrorMessage text={error}/>
                <nav className={listStyles.folderNavigation}>
                    <BackButton goBack={goBack} parentId={info.parentId}/>
                </nav>
                <Title {...info} />
                {
                    folders.map((folder) => (
                        <Folder
                            removeItem={async (e) => await this.removeFolderItem(folder, e)}
                            renameItem={async (newName) => await this.renameFolderItem(folder.id, newName,folder.name)}
                            key={folder.id}
                            name={folder.name}
                            onClick={() => loadFolderDetails(folder)}
                        />
                    ))
                }
                {
                    files.map(({name, id}) => (
                        <File key={id}
                              name={name}
                              renameItem={async (newName) => await this.renameFileItem(id, newName,name)}
                              removeItem={async (e) => await this.removeFileItem(id, e)}/>
                    ))
                }
            </div>
        );
    }
}

export default function connectList(actions) {
    return connect(
        ({list}) => list,
        (dispatch) => bindActionCreators(actions, dispatch)
    )(List);
}
