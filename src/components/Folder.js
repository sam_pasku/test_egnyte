import React from "react";

import styles from "./Folder.scss";
import ListItem from "./list.item";

export default function Folder({ name, onClick,removeItem,renameItem}) {
    return <div className={ styles.folder } onClick={ onClick }>
        <ListItem name={name} removeItem={removeItem} renameItem={renameItem}/>
    </div>;
}
