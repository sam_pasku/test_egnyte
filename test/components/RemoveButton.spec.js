import React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";

import RemoveButton from "../../src/components/buttons/remove";

describe("RemoveButton component", () => {
    it("triggers action on click", () => {
        const wrapper = shallow(<RemoveButton />);
        wrapper.find('button').simulate('click',{preventDefault:()=>{},stopPropagation:()=>{},});
        expect(wrapper.state('isPending')).to.equal(true);
    });

    it('make sure have button tag', function () {
        const wrapper = shallow(<RemoveButton />);
        expect(wrapper.find('button')).to.have.length(1);
    });

});
