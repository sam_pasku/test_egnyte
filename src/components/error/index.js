import React from "react";

import styles from "./index.scss";

export class ErrorMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isVisible: false};
    }

    componentWillReceiveProps() {
        this.setState({isVisible: true});
        setTimeout(() => {
            this.setState({isVisible: false});
        }, 2000)
    }

    render() {
        const {isVisible} = this.state;
        const {text} = this.props;

        return isVisible && text? (
            <div className={styles.container}>
                {text}
            </div>
        ) : null;
    }
}

