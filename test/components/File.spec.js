import React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";

import File from "../../src/components/File";
import ListItem from "../../src/components/list.item";

describe("File component", () => {
    it('renders provided filename', function () {
        const filename = "test-name.jpg";
        const wrapper = shallow(<File  name={ filename }/>);
        expect(wrapper.find('ListItem')).to.have.length(1);
    });
    it('should have props for name', function () {
        const filename = "test-name.jpg";
        const wrapper = shallow(<File name={ filename } />);
        expect(wrapper.props().name).to.be.defined;
    });
});
