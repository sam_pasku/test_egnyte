import React from "react";

import styles from "./index.scss";

export default class RemoveButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isPending: false};
    }

    action(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            isPending: true
        });
        setTimeout(() => {
            this.props.removeItem().then(() => {
                console.log("finish delete");
            }).catch((e)=>{
                this.setState({
                    isPending: false
                });
                alert(e);
            });

        }, 1500)//pretend ajax request will take no less 1.5s

    }

    render() {
        let isPending = this.state.isPending;
        const el = isPending ? <img src="https://media.giphy.com/media/3o7TKtnuHOHHUjR38Y/source.gif"/> :
            <button onClick={(e) => this.action(e)}>Remove</button>;

        return (
            <div className={styles['btn-container']}>
                {el}
            </div>
        )
    }

}
