const API_PATH = "/api";

function createGet(fetch) {
    return (path) => (id) => fetch(preparePath(path, id)).then((response) => response.json());
}

function createDelete(fetch) {
    return (path) => async (id) => {
        let response = await fetch(preparePath(path, id), {method: 'DELETE'});
        if (response.status == 404) throw new Error(response.statusText)

    }
}

function createEdit(fetch) {
    return (path) => async (data) => {
        let response = await fetch(preparePath(path, data.id) + "/rename", {
            method: 'POST',
            body: JSON.stringify(data)
        });//it should be PUT request
        if (response.status == 404) throw new Error(response.statusText)
        return response;
    }
}

function preparePath(path, id = "") {
    return `${ API_PATH }/${ path }/${ id }`;
}

export function createApi(fetch) {
    const get = createGet(fetch);
    const deleteItem = createDelete(fetch);
    const renameItem = createEdit(fetch);

    return {
        folder: () => ({
            get: get("folder"),
            renameItem: renameItem("folder"),
            deleteItem: deleteItem("folder")
        }),
        file: () => ({
            get: get("file"),
            renameItem: renameItem("file"),
            deleteItem: deleteItem("file")
        }),
        root: () => ({
            get: get("root")
        })
    }
}
