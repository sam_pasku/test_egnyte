import React from "react";

import styles from "./index.scss";
import RemoveButton from "../buttons/remove";
import RenameSaveButton from "../buttons/rename/save";

export default class ListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isEdit: false, newName: null};

        this.handleChange = this.handleChange.bind(this);
        this.handleInputClick = this.handleInputClick.bind(this);
    }

    onRenameEnabled(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            isEdit: true,
            newName: this.props.name
        });
    }

    async onRenameACt() {
        try {
            await this.props.renameItem(this.state.newName);
        } catch (e) {
            throw new Error(e);
        }finally {
            this.setState({
                isEdit: false
            });
        }


    }

    handleInputClick(e) {
        e.preventDefault();
        e.stopPropagation();
    }

    handleChange(event) {
        this.setState({newName: event.target.value});
    }

    render() {
        let {name, removeItem} = this.props,
            {newName, isEdit} = this.state,
            _el = isEdit ?
                <input autoFocus onClick={this.handleInputClick} value={newName} onChange={this.handleChange}/> :
                <span>{name}</span>,
            _actBtn = isEdit ? <RenameSaveButton renameItem={async () => await this.onRenameACt()}/> :
                <button onClick={(e) => this.onRenameEnabled(e)}>Rename</button>;

        return (
            <div className={styles['list-item']}>
                {_el}
                <div className={styles.actions}>
                    <RemoveButton removeItem={removeItem}/>
                    {_actBtn}
                </div>
            </div>
        );
    }
}
