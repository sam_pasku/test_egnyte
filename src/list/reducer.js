import {
    FOLDER_INFO_FETCHED,
    FOLDERS_DETAILS_FETCHED,
    FILES_DETAILS_FETCHED,
    FOLDER_RENAME,
    FILE_RENAME,
    FOLDER_REMOVE,
    FILE_REMOVE
} from "./actions";

export default function reducer(state = {folders: [], files: [], info: {}}, {type, payload}) {
    switch (type) {
        case FOLDER_INFO_FETCHED:
            return {...state, info: payload};
        case FOLDERS_DETAILS_FETCHED:
            return {...state, folders: payload};
        case FILES_DETAILS_FETCHED:
            return {...state, files: payload};
        case FILE_REMOVE:
            return {...state, files: state.files.filter((el) => el.id != payload.id)};
        case FOLDER_REMOVE:
            return {...state, folders: state.folders.filter((el) => el.id != payload.id)};
        case FOLDER_RENAME: {
            let list = state.folders;
            for (let i = 0; i < list.length; i++) {
                if (list[i].id == payload.id) {
                    Object.assign(list[i], payload);
                    break;
                }
            }
            return {...state, folders: list.concat([])};
        }
        case FILE_RENAME: {
            let list = state.files;
            for (let i = 0; i < list.length; i++) {
                if (list[i].id == payload.id) {
                    Object.assign(list[i], payload);
                    break;
                }
            }
            return {...state, files: list.concat([])};
        }

        default:
            return state;
    }
}
