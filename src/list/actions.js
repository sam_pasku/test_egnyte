export const FOLDER_INFO_FETCHED = "folderInfoFetched";
export const FOLDERS_DETAILS_FETCHED = "foldersDetailsFetched";
export const FILES_DETAILS_FETCHED = "filesDetailsFetched";
export const FILE_REMOVE = "fileRemove";
export const FOLDER_REMOVE = "folderRemove";
export const FOLDER_RENAME = "folderRename";
export const FILE_RENAME = "fileRename";

export default function createActions(api) {
    return {
        renameFolder: renameFolder(api),
        renameFile: renameFile(api),
        removeFolder: removeFolder(api),
        removeFile: removeFile(api),
        loadFolderDetails: createLoadFolderDetails(api),
        fetchRootData: createFetchRootData(api),
        goBack: createGoBack(api)
    }
};

export const createFetchRootData = (api) => () => (dispatch) =>
    api.root().get().then((folder) =>
        dispatch(createLoadFolderDetails(api)(folder))
    );

export const createGoBack = (api) => (parentId) => (dispatch) => {
    api.folder().get(parentId).then((folder) => {
        dispatch(createLoadFolderDetails(api)(folder));
    });
};

export const removeFile = (api) => (id) => async (dispatch) => {
    try {
        await api.file().deleteItem(id);
        dispatch(removeFileItem(api)(id));
    } catch (e) {
        throw new Error(e);
    }
};

export const renameFolder = (api) => (data) => async (dispatch) => {
    try {
        await api.folder().renameItem(data);
        dispatch(renameFolderItem(api)(data));
    } catch (e) {
        throw new Error(e);
    }
};
export const renameFile = (api) => (data) => async (dispatch) => {
    try {
        await api.file().renameItem(data);
        dispatch(renameFileItem(api)(data));
    } catch (e) {
        throw new Error(e);
    }
};
export const removeFolder = (api) => (id) => async (dispatch) => {
    try {
        await api.folder().deleteItem(id);
        dispatch(removeFolderItem(api)(id));
    } catch (e) {
        throw new Error(e);
    }
};

const createLoadFolderDetails = (api) => (folder) => (dispatch) => {
    return Promise.all([
        dispatch({type: FOLDER_INFO_FETCHED, payload: folder}),
        dispatch(createFetchFoldersList(api)(folder.folders)),
        dispatch(createFetchFilesDetails(api)(folder.files))
    ]);
};

const createFetchFoldersList = (api) => (ids) => {
    const all = Promise.all(ids.map((id) => api.folder().get(id)));

    return ({type: FOLDERS_DETAILS_FETCHED, payload: all});
};

const createFetchFilesDetails = (api) => (ids) => {
    const all = Promise.all(ids.map((id) => api.file().get(id)));

    return ({type: FILES_DETAILS_FETCHED, payload: all});
};

const removeFileItem = (api) => (id) => {
    return ({type: FILE_REMOVE, payload: {id}});
};

const removeFolderItem = (api) => (id) => {
    return ({type: FOLDER_REMOVE, payload: {id}});
};
const renameFolderItem = (api) => (data) => {
    return ({type: FOLDER_RENAME, payload: data});
};
const renameFileItem = (api) => (data) => {
    return ({type: FILE_RENAME, payload: data});
};
